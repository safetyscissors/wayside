//
//  LocationCell.swift
//  wayside
//
//  Created by Jason Lagaac on 17/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import UIKit

class LocationTableCell : UITableViewCell {
    
    // Location name label
    @IBOutlet var locationNameLbl : UILabel?
    
    // Location address label
    @IBOutlet var addressLbl : UILabel?
    
    // Rating label
    @IBOutlet var ratingLbl : UILabel?
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        ratingLbl?.clipsToBounds = true
        ratingLbl?.layer.cornerRadius = ratingLbl!.frame.width / 2
        ratingLbl?.backgroundColor = UIColor.grayColor()
    }
}


