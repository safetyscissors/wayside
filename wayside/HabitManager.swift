//
//  HabitManager.swift
//  wayside
//
//  Created by Jason Lagaac on 1/02/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import Foundation


class HabitManager {
    
    class func saveHabit (keyword : String, hour : Int, minute: Int, day : Int, success : () -> Void) {
        var newHabit = Habit()

        newHabit.keyword = keyword
        newHabit.day = day
        
        newHabit.hour = hour
        newHabit.minute = minute
        
        newHabit.saveInBackgroundWithBlock({ (saved, error) -> Void in
            if saved == true && error == nil {
                success()
            } else {
                
            }
        })
        
        newHabit.pinInBackgroundWithBlock(nil)
    }
    
    class func getAllHabits(success : ([Habit]) -> Void, failed : (NSError) -> Void) {
        var query = PFQuery(className: "Habit")
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            if error == nil {
                success(objects as [Habit])
            } else {
                failed(error)
            }
        }
    }
}