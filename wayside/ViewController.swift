//
//  ViewController.swift
//  wayside
//
//  Created by Jason Lagaac on 15/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import UIKit
import QuadratTouch

class ViewController: UIViewController, LocationManagerProtocol, HabitSelectedProtocol {
    
    @IBOutlet var scheduleTableView : UITableView?
    
    @IBOutlet var scheduleScrollView : UIScrollView?
    
    var habitsLoaded : Bool = false
    
    var selectedHabit : Habit?
    
    var currentLocation : CLLocation?
    
    var scheduleTableViewControllers : [ScheduleTableViewController] = [ScheduleTableViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        LocationManager.sharedInstance.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if (self.habitsLoaded == false) {
            self.loadScheduleScrollView()
            
            HabitManager.getAllHabits({ (habits) -> Void in
                for var i = 0; i < habits.count; i++ {
                    var day = habits[i]["day"] as Int
                    let tvc : ScheduleTableViewController = self.scheduleTableViewControllers[day - 1] as ScheduleTableViewController
                    
                    tvc.habits.append(habits[i])
                    tvc.tableView.reloadData()
                    tvc.delegate = self
                }
            }, failed: { (err) -> Void in
                println(err);
            })
            
            habitsLoaded = true;
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadScheduleScrollView() {
        let w : CGFloat = CGFloat(UIScreen.mainScreen().bounds.size.width)
        let h : CGFloat = CGFloat(UIScreen.mainScreen().bounds.size.height)

        self.scheduleScrollView?.contentSize = CGSizeMake(w * 7, 0);
        
        for (var i = 0; i < 7; i++) {
            var posX = (CGFloat(i) * w)
            let newRect = CGRect(x: posX, y: 0, width: w, height: h)
            self.scheduleTableViewControllers.append(ScheduleTableViewController.instanceFromStoryboard())
            var vc : ScheduleTableViewController = self.scheduleTableViewControllers.last!
            vc.view.frame = newRect
            self.scheduleScrollView?.addSubview(vc.view)
        }
    
        self.scheduleScrollView?.layoutIfNeeded()
    }
    
    
    /// MARK: Location manager delegates
    func locationManagerDidUpateLocations(locations: [AnyObject]) {
        println(locations)
        self.currentLocation = locations.first as? CLLocation
    }
    
    func locationManagerFailedWithError(error: NSError) {
        println(error)
    }
    
    @IBAction func createNewHabit() {
        self.performSegueWithIdentifier("CreateNewHabit", sender: nil);
    }
    
    
    func habitSelected(habit: Habit) {
        self.selectedHabit = habit
        self.performSegueWithIdentifier("ShowRecommendations", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ShowRecommendations") {
            var vc = segue.destinationViewController as RecommendationViewController;
            vc.selectedHabit = self.selectedHabit
            vc.currentLocation = self.currentLocation
        }
    }

}

