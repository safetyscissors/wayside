//
//  LocationManager.swift
//  wayside
//
//  Created by Jason Lagaac on 18/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import Foundation
import QuadratTouch

protocol LocationManagerProtocol {
    
    func locationManagerDidUpateLocations(locations : [AnyObject])
    
    // Location manager failed
    func locationManagerFailedWithError(error : NSError)

}

class LocationManager : NSObject, CLLocationManagerDelegate {
    
    // Location manager delegate
    var delegate : LocationManagerProtocol?
    
    // Location manager
    var manager : CLLocationManager = CLLocationManager()
    
    /// Singleton instance
    class var sharedInstance : LocationManager {
        struct Static {
            static let instance : LocationManager = LocationManager()
        }
        
        return Static.instance
    }
    
    override init() {
        super.init()
        manager.delegate = self
    }
    
    /// CoreLocation Delegate Actions
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {

        if delegate != nil {
            delegate?.locationManagerDidUpateLocations(locations)
        }
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        if delegate != nil {
            delegate?.locationManagerFailedWithError(error)
        }
    }
}