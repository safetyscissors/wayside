//
//  VenueTableCell.swift
//  wayside
//
//  Created by Jason Lagaac on 31/03/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import UIKit

class VenueTableCell : UITableViewCell {
    @IBOutlet var venueNameLbl : UILabel?
    
    @IBOutlet var addressLbl : UILabel?
    
    var name : String = ""
    
    var address : String = ""
    
    var rating : Float?

    func setWithData(data : JSON) {
        var venue = data["venue"]
        var addressLines = venue["location"]["formattedAddress"].arrayValue
        
        name = venue["name"].stringValue
        address = ""
        
        for var i = 0; i  < addressLines.count; i++ {
            var line = addressLines[i]
            self.address += "\(line.stringValue)"
            
            if i < addressLines.count - 1 {
                self.address += "\n"
            }
        }
        
        venueNameLbl?.text = name
        addressLbl?.text = address
    }
    
}