//
//  CreateHabitViewController.swift
//  wayside
//
//  Created by Jason Lagaac on 26/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import UIKit

class CreateHabitViewController : UIViewController {
    
    @IBOutlet var dayButton : UIButton?
    
    @IBOutlet var timeButton : UIButton?
    
    @IBOutlet var keywordField : UITextField?
    
    @IBOutlet var doneButton : UIBarButtonItem?
    
    var dismissKeywordFieldTapGesture : UITapGestureRecognizer?
    
    var selectedTime : NSDate = NSDate()
    
    var selectedDay : Int = 0
    
    var dayList = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    
    override func viewDidLoad() {
        self.navigationController?.navigationBarHidden = false
        self.dismissKeywordFieldTapGesture = UITapGestureRecognizer(target: self, action: Selector("dismissKeywordField"))
        self.view.addGestureRecognizer(self.dismissKeywordFieldTapGesture!)
        
        self.keywordField?.addTarget(self, action: Selector("textFieldDidChange:"), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        self.doneButton?.enabled = false
        loadDateData()
    }
    
    func loadDateData() {
        var today = NSDate();
        timeButton?.setTitle(formatTime(today), forState: UIControlState.Normal)
        
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "EEEE"
        let todayName = formatter.stringFromDate(today)
        dayButton?.setTitle(todayName, forState: UIControlState.Normal)
    }
    
    func dismissKeywordField() {
        self.keywordField?.resignFirstResponder()
    }
    
    @IBAction func showDaySelector() {
        self.keywordField?.resignFirstResponder()
        
        ActionSheetStringPicker.showPickerWithTitle("Set a day",
            rows: dayList,
            initialSelection: self.selectedDay - 1,
            doneBlock: { (picker, selectedIndex, selectedValue) -> Void in
                self.selectedDay = selectedIndex + 1
                self.dayButton?.setTitle(self.dayList[selectedIndex], forState: UIControlState.Normal)
            }, cancelBlock: { (picker) -> Void in
                
            }, origin: self.view)
    }
    
    @IBAction func showTimeSelector() {
        self.keywordField?.resignFirstResponder()
        
        ActionSheetDatePicker.showPickerWithTitle("Select a time",
            datePickerMode: UIDatePickerMode.Time,
            selectedDate: self.selectedTime,
            doneBlock: { (picker, selectedDate, origin) -> Void in
                self.selectedTime = selectedDate as NSDate
                self.timeButton?.setTitle(self.formatTime(selectedDate as NSDate), forState: UIControlState.Normal)
            }, cancelBlock: { (picker) -> Void in
                
            }, origin: self.view)
    }
    
    @IBAction func dismiss() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveHabit() {
        self.keywordField?.resignFirstResponder()
        var keyword = self.keywordField!.text.lowercaseString
        
        if keyword.utf16Count > 0 {
            SVProgressHUD.showWithMaskType(SVProgressHUDMaskType.Gradient)
            var components = dateComponents(self.selectedTime)
            
            HabitManager.saveHabit(self.keywordField!.text.lowercaseString,
                hour: components.hour,
                minute: components.minute,
                day: self.selectedDay, success: { () -> Void in
                    self.dismiss()
                    SVProgressHUD.showSuccessWithStatus(nil)
            })
        }
    }
    
    func dateComponents(date : NSDate) -> NSDateComponents {
        return NSCalendar.currentCalendar().components(NSCalendarUnit.CalendarUnitWeekday |
            NSCalendarUnit.CalendarUnitHour |
            NSCalendarUnit.CalendarUnitMinute, fromDate: date)
    }
    
    func formatTime(date : NSDate) -> NSString {
        var components = dateComponents(date)
        self.selectedDay = components.weekday
        
        var timePeriod = (components.hour > 12 ? "PM" : "AM")
        var currHour = (components.hour > 12 ? components.hour - 12 : components.hour)
        var currentFormattedTime = "\(currHour):" + String(format: "%02d", components.minute) + " \(timePeriod)"
        
        return currentFormattedTime;
    }
    
    func textFieldDidChange(textField : UITextField) {
        self.doneButton?.enabled = (textField.text.utf16Count == 0 ? false : true)
    }
}
