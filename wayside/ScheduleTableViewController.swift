//
//  ScheduleTableViewController.swift
//  wayside
//
//  Created by Jason Lagaac on 26/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import Foundation

protocol HabitSelectedProtocol {
    // protocol definition goes here
    func habitSelected(habit : Habit)
}

class ScheduleTableViewController : UITableViewController {
    
    var delegate : HabitSelectedProtocol?
    
    var habits : [Habit] = [Habit]()
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // UITableView delegate actions
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var habit = habits[indexPath.row]
        self.delegate?.habitSelected(habit)
    }
    
    // UITableView data source actions
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.habits.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : HabitTableViewCell = tableView.dequeueReusableCellWithIdentifier("HabitTableViewCell") as HabitTableViewCell
        let habit = self.habits[indexPath.row]
        cell.displayWithHabit(habit)
        
        return cell
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70.0
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    class func instanceFromStoryboard() -> ScheduleTableViewController {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewControllerWithIdentifier("ScheduleTableViewController") as ScheduleTableViewController
    }
}