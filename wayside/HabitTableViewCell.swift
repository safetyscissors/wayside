//
//  HabitTableViewCell.swift
//  wayside
//
//  Created by Jason Lagaac on 22/03/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import Foundation
import UIKit

class HabitTableViewCell : UITableViewCell {
    // Display the time
    @IBOutlet var timeLbl : UILabel?
    
    // Display the habit
    @IBOutlet var habitLbl : UILabel?
    
    func displayWithHabit(habit : Habit) {
        let hour = habit.hour
        let minute = habit.minute
        let keyword = habit.keyword
        
        self.timeLbl?.text = String(format: "%02d:%02d", hour, minute)
        self.habitLbl?.text = keyword
    }
}