//
//  RecommendationViewController.swift
//  wayside
//
//  Created by Jason Lagaac on 30/03/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import UIKit
import QuadratTouch

class RecommendationViewController : UITableViewController {
    
    var selectedHabit : Habit?
    
    var currentLocation : CLLocation?
    
    var venues : [JSON] = [JSON]()
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        var lat = currentLocation!.coordinate.latitude
        var lng = currentLocation!.coordinate.longitude
        
        let session = Session.sharedSession()
        let ll =  "\(lat),\(lng)"
        var parameters = [Parameter.query:selectedHabit!.keyword!,
            Parameter.ll:ll];

        let searchTask = session.venues.explore(parameters) {
            (result) -> Void in
            var response : JSON = JSON(result.response!) as JSON
            var data = response["groups"].arrayValue.first
            
            if let items = data?["items"].arrayValue {
                self.venues = items
                println(self.venues)
                self.tableView.reloadData()
            }
        }
        
        searchTask.start()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venues.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : VenueTableCell = tableView.dequeueReusableCellWithIdentifier("VenueCell") as VenueTableCell
        
        cell.setWithData(venues[indexPath.row])
        
        return cell
    }
}
