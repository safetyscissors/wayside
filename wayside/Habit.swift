//
//  Habit.swift
//  wayside
//
//  Created by Jason Lagaac on 27/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import Foundation


class Habit : PFObject, PFSubclassing {
    
    var keyword : String? {
        get {
            return self["keyword"] as? String
        }
        
        set {
            self["keyword"] = newValue
        }
    }
    
    var hour : Int {
        get {
            return self["hour"] as Int
        }
        
        set {
            self["hour"] = newValue
        }
    }
    
    var minute : Int {
        get {
            return self["minute"] as Int
        }
        
        set {
            self["minute"] = newValue
        }
    }
    
    var day : Int {
        get {
            return self["day"] as Int
        }
        
        set {
            self["day"] = newValue
        }
    }
    
    class func parseClassName() -> String! {
        return "Habit"
    }
}