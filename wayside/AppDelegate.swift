//
//  AppDelegate.swift
//  wayside
//
//  Created by Jason Lagaac on 15/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

import UIKit
import QuadratTouch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Override point for customization after application launch.
        
        // Initialise Parse
        setupParse()
        
        // Initialise Foursquare
        setupFoursquare()
        
        // Start location manager
        LocationManager.sharedInstance.manager.requestAlwaysAuthorization()
        LocationManager.sharedInstance.manager.startUpdatingLocation()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setupParse() {
        Habit.registerSubclass()
        Parse.enableLocalDatastore()
        Parse.setApplicationId(APIKey.parseAppID,
                               clientKey: APIKey.parseClientKey)
    }
    
    func setupFoursquare() {
        let client = Client(clientID: APIKey.fsClientID,
                            clientSecret: APIKey.fsClientSecret,
                            redirectURL: APIKey.fsRedirectURL)

        var configuration = Configuration(client:client)
        Session.setupSharedSessionWithConfiguration(configuration)

    }


}

